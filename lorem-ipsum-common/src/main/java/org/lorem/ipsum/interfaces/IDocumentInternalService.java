package org.lorem.ipsum.interfaces;

import org.lorem.ipsum.model.Document;

/**
 * Every external endpoint that handles documents implements this service for getting/adding/deleting/editing documents
 * 
 * @author chris
 *
 */
public interface IDocumentInternalService extends IDocumentService {

	/**
	 * Add the given document, returns the document id
	 * 
	 * @param document
	 * @return
	 */
	public int addDocument(Document document);
	
	/**
	 * Delete a document with the given id, return success/fail boolean
	 * 
	 * @param documentId
	 * @return
	 */
	public boolean deleteDocument(int documentId);
	
	
	/**
	 * Get the document given by the document id
	 * 
	 * @param documentId
	 * @return
	 */
	public Document getDocument(int documentId);
	
	/**
	 * Update the given document
	 * 
	 * @param document
	 * @return
	 */
	public Document updateDocument(Document document);
	
}
