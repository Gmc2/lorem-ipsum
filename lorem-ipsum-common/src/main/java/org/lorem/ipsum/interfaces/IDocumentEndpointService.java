package org.lorem.ipsum.interfaces;

import org.lorem.ipsum.exceptions.LoremIpsumException;
import org.lorem.ipsum.model.Document;
import org.lorem.ipsum.model.Revision;

/**
 * Every external endpoint that handles documents implements this service for getting/adding/deleting/editing documents
 * 
 * @author chris
 *
 */
public interface IDocumentEndpointService extends IDocumentService {

	/**
	 * Add a new document with the given values, returns the document id
	 * 
	 * @param title
	 * @param htmlContent
	 * @param summary
	 * @return
	 */
	public int addDocument(String title, String htmlContent, String summary) throws LoremIpsumException;
	
	/**
	 * Delete a document with the given id, return success/fail boolean
	 * 
	 * @param documentId
	 * @return
	 */
	public boolean deleteDocument(int documentId) throws LoremIpsumException;
	
	
	/**
	 * Get the document given by the document id
	 * 
	 * @param documentId
	 * @return
	 */
	public Document getDocument(int documentId) throws LoremIpsumException;
	
	/**
	 * Edit a given document with the specified revision
	 * 
	 * @param documentId
	 * @param revision
	 * @return
	 */
	public Document editDocument(int documentId, Revision revision) throws LoremIpsumException;
	
}
