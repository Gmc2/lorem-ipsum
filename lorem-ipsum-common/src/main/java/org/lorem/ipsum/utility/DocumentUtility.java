package org.lorem.ipsum.utility;

import java.util.Set;

import org.lorem.ipsum.model.Document;
import org.lorem.ipsum.model.Revision;
import org.lorem.ipsum.model.Revision.Edit;
import org.lorem.ipsum.model.Revision.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utilities for document class/revisions
 * 
 * @author chris
 *
 */
public class DocumentUtility {
	
	private static Logger logger = LoggerFactory.getLogger(DocumentUtility.class);

	/**
	 * Perform a revision, according to revision object, on given document
	 * 
	 * @param document
	 * @param revision
	 */
	public static void revise(Document document, Revision revision) {
		
		//get edits from revision
		Set<Edit> edits = revision.getEdits();
		
		//for each edit, perform revision
		for(Edit e : edits) {
			Field field = e.getField();
			String newContent = e.getRevisedContent();
			
			//debug revisions
			DocumentUtility.logger.debug("Making revision field={} => \"{}\"", field, newContent);
			
			if(Field.TITLE.equals(field)) {
				document.setTitle(newContent);
			} else if(Field.HTML_CONTENT.equals(field)) {
				document.setHtmlContents(newContent);
			} else if(Field.SUMMARY.equals(field)) {
				document.setSummary(newContent);
			}
			
		}
		
	}
	
}
