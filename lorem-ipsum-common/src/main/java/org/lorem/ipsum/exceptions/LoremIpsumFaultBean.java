package org.lorem.ipsum.exceptions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="error")
//set the accessor type to field: prevents an error where field annotations on xml elements causes an exception with JAXB
@XmlAccessorType(XmlAccessType.FIELD)
public class LoremIpsumFaultBean {

	@XmlElement(name="message")
	private String errorMessage = "";

	public LoremIpsumFaultBean() {
		
	}
	
	public LoremIpsumFaultBean(LoremIpsumException loremIpsumException) {
		this.errorMessage = loremIpsumException.getErrorMessage();
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
