package org.lorem.ipsum.exceptions;


public class DocumentNotFoundException extends LoremIpsumException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DocumentNotFoundException() {
		
	}

	public DocumentNotFoundException(String message) {
		super(message);
	}
	
	public DocumentNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
