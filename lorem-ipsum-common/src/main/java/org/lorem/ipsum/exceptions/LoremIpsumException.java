package org.lorem.ipsum.exceptions;


public class LoremIpsumException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String message = "";

	private Throwable cause = null;
	
	private String errorMessage = "";
	
	public LoremIpsumException() {
		
	}

	public LoremIpsumException(LoremIpsumException exception) {
		super(exception);
	}
	
	public LoremIpsumException(String message) {
		this.message = message;
		this.errorMessage = this.message;
	}
	
	public LoremIpsumException(String message, Throwable cause) {
		this(message);
		this.cause = cause;
		
		StringBuilder builder = new StringBuilder();
		builder.append(this.message);
		if(this.cause != null) {
			builder.append("\n");
			builder.append("caused by: ");
			builder.append(this.cause.getMessage());
			builder.append("\n");
			for(StackTraceElement ste : this.cause.getStackTrace()) {
				builder.append(ste.toString());
				builder.append("\n");
			}
		}	
		this.errorMessage = builder.toString();
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}
