package org.lorem.ipsum.model;

public enum DocumentStatus {

	NEW,
	AWAITING_APPROVAL,
	APPROVED,
	REJECTED
	;
	
}
