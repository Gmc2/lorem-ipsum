package org.lorem.ipsum.model;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A document is an object that has a title, summary, and some HTML content block.
 * 
 * @author chris
 *
 */
//explicit set document table name
@Entity(name="document")
//mark this object as cachable
@Cacheable
//explicitly set root element name
@XmlRootElement(name="document")
//set the accessor type to field: prevents an error where field annotations on xml elements causes an exception with JAXB
@XmlAccessorType(XmlAccessType.FIELD)
//document is also a type
@XmlType(name="documentType")
public class Document implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//only accept documents with a document id of 0 or greater
	@Min(value=0, message="{org.lorem.ipsum.model.document.documentnumber.min}")
	//this is an id field
	@Id
	//the generated strategy is a sequence (configured by SQL implementor), using AUTO here will cause the id to not be updated in the 
	//entity manager upon persistence
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "documentIdSequenceGenerator")
    @SequenceGenerator(name = "documentIdSequenceGenerator", sequenceName = "DOCUMENT_ID_SEQUENCE")
	//do not require the element, allow it to be null, and set the default value to -1, this will allow new documents to be created without complaint
	@XmlElement(required=false, nillable=true, defaultValue="-1")
	/**
	 * The document number is the document id used as the identity of the document
	 */
	private int documentNumber;

	//no null titles
	@NotNull
	//the title must contain at least one character, use or.lorem.ipsum.model.document.title.size as the property key for i18n/interpolation
	@Size(min=1, message="{org.lorem.ipsum.model.document.title.size}")
	//column title, do not allow null values in database
	@Column(nullable=false, name="title")
	//this should be an xml element
	@XmlElement
	/**
	 * The title of the document
	 */
	private String title;
	
	//no null contents
	@NotNull
	//the contents must contain at least one character, use or.lorem.ipsum.model.document.htmlContents.size as the property key for i18n/interpolation
	@Size(min=1, message="{org.lorem.ipsum.model.document.htmlContents.size}")
	//column htmlContents, do not allow null values in database
	@Column(nullable=false, name="htmlContents")
	//this should be an xml element
	@XmlElement
	/**
	 * The document's HTML formatted contents 
	 */
	private String htmlContents;
	
	//no null summaries
	@NotNull
	//the summary must contain at least five characters, use or.lorem.ipsum.model.document.summary.size as the property key for i18n/interpolation
	@Size(min=5, message="{org.lorem.ipsum.model.document.summary.size}")
	//column summary, do not allow null values in database
	@Column(nullable=false, name="summary")
	//this should be an xml element	
	@XmlElement
	/**
	 * The document's summary/description
	 */
	private String summary;
	
	//should always provide a status
	@NotNull
	@Column(name="status")
	@XmlAttribute(name="status", required=false)
	/**
	 * The state of the workflow that the document is in, defaults to NEW.
	 */
	private DocumentStatus status = DocumentStatus.NEW;

	public DocumentStatus getStatus() {
		return status;
	}

	public void setStatus(DocumentStatus status) {
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getHtmlContents() {
		return htmlContents;
	}

	public void setHtmlContents(String htmlContents) {
		this.htmlContents = htmlContents;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public int getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(int documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + documentNumber;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Document other = (Document) obj;
		if (documentNumber != other.documentNumber)
			return false;
		return true;
	}
		
}
