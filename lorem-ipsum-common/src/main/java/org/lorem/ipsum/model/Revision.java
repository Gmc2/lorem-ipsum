package org.lorem.ipsum.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A revision is a message/notification of change to a provided {@link Document}.
 * 
 * @author chris
 *
 */
//explicit xml element
@XmlRootElement(name="revision")
//set the accessor type to field: prevents an error where field annotations on xml elements causes an exception with JAXB
@XmlAccessorType(XmlAccessType.FIELD)
public class Revision implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The different types of field provided inside of a {@link Document}
	 * 
	 * @author chris
	 *
	 */
	public static enum Field {
		TITLE,
		HTML_CONTENT,
		SUMMARY
		;
	}
	
	/**
	 * An edit is a change to a single {@link Field} within a {@link Document}
	 * 
	 * @author chris
	 *
	 */
	//explicit xml element
	@XmlRootElement(name="edit")
	//set the accessor type to field: prevents an error where field annotations on xml elements causes an exception with JAXB
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Edit implements Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;	
		
		//the field value should be an xml attribute
		@XmlAttribute(name="field")
		/**
		 * Field to be changed
		 * 
		 */
		private Field field;
		
		//the new content after the revision
		@XmlElement(name="content")
		/**
		 * Revised content to be added in field
		 * 
		 */
		private String revisedContent;

		public Edit() {
			
		}
		
		/**
		 * Quick constructor for a revision with {@link Field} and revised content provided
		 * 
		 * @param field
		 * @param revisedContent
		 */
		public Edit(Field field, String revisedContent) {
			this();
			
			this.field = field;
			this.revisedContent = revisedContent;
		}
		
		public Field getField() {
			return field;
		}
		
		public void setField(Field field) {
			this.field = field;
		}
		
		public String getRevisedContent() {
			return revisedContent;
		}
		
		public void setRevisedContent(String revisedContent) {
			this.revisedContent = revisedContent;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((field == null) ? 0 : field.hashCode());
			return result;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Edit other = (Edit) obj;
			if (field != other.field)
				return false;
			return true;
		}
	}

	//the list wrapper is called "edits"
	@XmlElementWrapper(name="edits")
	//the individual list items are each called "edit"
	@XmlElement(name="edit")
	/**
	 * List of edits to be made.  A set is used here becuase having two edits to the same field 
	 * within a single revision is pointless.  (The first revision would be rendered moot by the
	 * second revision.)
	 * 
	 */
	private Set<Edit> edits = new HashSet<Revision.Edit>();

	public Set<Edit> getEdits() {
		return edits;
	}

	public void setEdits(Set<Edit> edits) {
		this.edits = edits;
	}
	
}
