package org.lorem.ipsum.services;

import org.lorem.ipsum.exceptions.LoremIpsumException;
import org.lorem.ipsum.interfaces.IDocumentEndpointService;
import org.lorem.ipsum.logic.AbstractUsesDocumentServices;
import org.lorem.ipsum.model.Document;
import org.lorem.ipsum.model.Revision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AbstractEndpointProvider extends AbstractUsesDocumentServices<IDocumentEndpointService> implements IDocumentEndpointService {

	/**
	 * Logger (SLF4J)
	 */
	private static Logger logger = LoggerFactory.getLogger(AbstractEndpointProvider.class);
	
	/**
	 * JNDI name of service to look up, in this case one that implements IDocumentEndpoinService and provides a proxy/passthrough for the ESB
	 * (this is shared amongst the external http endpoints so that the ESB code is not duplicated)
	 * 
	 */
	private final String jndiServiceName = "lorem-ipsum-ear-0.0.1-SNAPSHOT/ESBDocumentEndpointProxyBean/remote";
	
	@Override
	public int addDocument(String title, String htmlContent, String summary) throws LoremIpsumException {
		//retrieve document service
		IDocumentEndpointService documentService = this.getDocumentService(this.jndiServiceName);
		
		//use document service
		int documentId = -1;
		try {
			documentId = documentService.addDocument(title, htmlContent, summary);
		} catch (LoremIpsumException exception) {
			this.handleException(exception, "adding");
		}		
				
		//return document id so that the consumer can use it
		return documentId;
	}

	@Override
	public boolean deleteDocument(int documentId) throws LoremIpsumException {
		//retrieve document service
		IDocumentEndpointService documentService = this.getDocumentService(this.jndiServiceName);
		
		//use document service
		boolean deleted = false;
		try {
			deleted = documentService.deleteDocument(documentId);
		} catch (LoremIpsumException exception) {
			this.handleException(exception, "deleting");
		}		
				
		//return the document deleted status
		return deleted;
	}

	@Override
	public Document getDocument(int documentId) throws LoremIpsumException {
		
		//retrieve document service
		IDocumentEndpointService documentService = this.getDocumentService(this.jndiServiceName);
		
		//use document service
		Document document = null;
		try {
			document = documentService.getDocument(documentId);
		} catch (LoremIpsumException exception) {
			this.handleException(exception, "getting");
		}			
		
		//return document
		return document;
	}

	@Override
	public Document editDocument(int documentId, Revision revision) throws LoremIpsumException {

		//retrieve document service
		IDocumentEndpointService documentService = this.getDocumentService(this.jndiServiceName);
		
		//use document service
		Document document = null;
		try {
			document = documentService.editDocument(documentId, revision);
		} catch (LoremIpsumException exception) {
			this.handleException(exception, "editing");
		}		
		
		//return document
		return document;
	}
	
	private void handleException(LoremIpsumException exception, String verb) throws LoremIpsumException {
		//log error
		AbstractEndpointProvider.logger.error("An error \"{}\" occurred while {} the document. (In service class: {})", new Object[]{exception.getMessage(), verb, this.getClass().getName(), exception});
		
		//for debugging purposes
		//exception.printStackTrace();
		
		//rethrow exception
		throw exception;		
	}

}
