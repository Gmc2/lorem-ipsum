package org.lorem.ipsum.messaging;

/**
 * Payload locations for ESB Messages
 * 
 * @author chris
 *
 */
public enum Payload {
	
	/**
	 * document id
	 */
	DOCUMENT_ID("documentId"),
	
	/**
	 * document (default body payload location)
	 */
	DOCUMENT(org.jboss.soa.esb.message.Body.DEFAULT_LOCATION),

	/**
	 * revision
	 */
	REVISION("revision"),
	
	/**
	 * constraint violations from jsr 303
	 */
	CONSTRAINT_VIOLATIONS("constraintViolations"),
	
	/**
	 * exceptions encountered during processing
	 */
	EXCEPTIONS("exceptionCollection")
	;
	
	private final String key;
	
	private Payload(String key) {
		this.key = key;
	}
	
	public String getKey() {
		return this.key;
	}
	
}
