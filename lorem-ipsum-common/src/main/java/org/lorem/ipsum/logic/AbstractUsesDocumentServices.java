package org.lorem.ipsum.logic;

import java.util.concurrent.Semaphore;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.lorem.ipsum.interfaces.IDocumentInternalService;
import org.lorem.ipsum.interfaces.IDocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractUsesDocumentServices<T extends IDocumentService> {
	
	/**
	 * Static/shared instance of an internal document service provider
	 * 
	 * @see IDocumentInternalService
	 */
	private T internalDocumentService = null;
	
	/**
	 * Only one thread should be managing the service lookup/acquire at a time 
	 */
	private Semaphore serviceLookupMutex = new Semaphore(1, true);
	
	/**
	 * Logger
	 */
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@SuppressWarnings("unchecked")
	protected T getDocumentService(String jndiName) {
		
		//initial check to prevent locking if the document service is not null
		//this is implemented for speedup so that the mutex is not handled and
		//no blocking occurs in this case.  however the possible side-effect
		//is that the second request uses the now not-null document service
		//before the logging finishes for the successful lookup attempt.
		if(this.internalDocumentService == null) {
		
			//acquire mutex lock so that only one service is handling the document service lookup at a time
			try {
				this.serviceLookupMutex.acquire();
			} catch (InterruptedException e) {
				this.logger.error("Lookup Mutex Aquire interupted: {}", e.getMessage(), e);
			}
			
			//lazy create internal document service through JNDI lookup for persistence provider
			//(checking null again here because it will change when we're in the lock)
			if(this.internalDocumentService == null) {
				try {
					//shortcut for new IntitialContext then context lookup
					internalDocumentService = (T)InitialContext.doLookup(jndiName);
					this.logger.debug("Found \"{}\" service bean through JNDI lookup.", jndiName);
				} catch (NamingException e) {
					this.logger.error("Failed to get document service from EJB3 lookup, service \"{}\" is in failure mode: {}", jndiName, e.getMessage());
				}			
			}
			
			//release mutex lock
			this.serviceLookupMutex.release();			
		}
		
		return this.internalDocumentService;
	}
}
