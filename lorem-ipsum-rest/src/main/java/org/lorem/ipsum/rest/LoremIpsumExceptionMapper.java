package org.lorem.ipsum.rest;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.lorem.ipsum.exceptions.DocumentNotFoundException;
import org.lorem.ipsum.exceptions.LoremIpsumFaultBean;
import org.lorem.ipsum.exceptions.LoremIpsumException;

@Provider
public class LoremIpsumExceptionMapper implements ExceptionMapper<LoremIpsumException> {

	@Override
	public Response toResponse(LoremIpsumException loremIpsumException) {
		
		ResponseBuilder responseBuilder = null;
		
		LoremIpsumFaultBean wrapper = new LoremIpsumFaultBean(loremIpsumException);
		
		if(loremIpsumException instanceof DocumentNotFoundException) {
			responseBuilder = Response.status(Status.NOT_FOUND);
		} else {
			responseBuilder = Response.serverError();
		}
		
		responseBuilder.entity(wrapper);
		
		return responseBuilder.build();
	}

	
	
}
