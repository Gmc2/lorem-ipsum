package org.lorem.ipsum.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.providers.jaxb.json.BadgerFish;
import org.lorem.ipsum.exceptions.LoremIpsumException;
import org.lorem.ipsum.model.Document;
import org.lorem.ipsum.model.Revision;
import org.lorem.ipsum.services.AbstractEndpointProvider;

//specify the path, relative to the path set in the web.xml servlet, for this restful endpoint
@Path("/document")
/**
 * Provides restful services for documents
 * 
 * @author chris
 *
 */
public class DocumentServiceRESTEndpoint extends AbstractEndpointProvider {
 
	public DocumentServiceRESTEndpoint() {
		
	}

	//accept only posts
	@POST
	//path will be /document/add
	@Path(value="/add")
	@Override
	public int addDocument(@FormParam("title") String title, @FormParam("htmlContent") String htmlContent, @FormParam("summary") String summary) throws LoremIpsumException {
		return super.addDocument(title, htmlContent, summary);
	}

	
	//accept only posts
	@POST
	//path will be /document/delete/id where id is the id of the document to delete, 
	//and matched by the path parameter to the documentId in the method 
	@Path(value="/delete/{id}")
	@Override
	public boolean deleteDocument(@PathParam("id") int documentId) throws LoremIpsumException {
		return super.deleteDocument(documentId);
	}
	
	//accepts only get http methods
	@GET
	//path will be /document/get/id where id is the id of the document to retrieve, 
	//and matched by the path parameter to the documentId in the method
	@Path(value="/get/{id}")
	//use badgerfish XML -> JSON encoding (http://wiki.open311.org/index.php?title=JSON_and_XML_Conversion#The_Badgerfish_Convention)
	@BadgerFish
	//produces JSON, XML, HTML and Plain Text depending on the consumer
	@Produces(value={MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML, MediaType.TEXT_HTML, MediaType.TEXT_PLAIN})
	@Override
	public Document getDocument(@PathParam("id") int documentId) throws LoremIpsumException {
		return super.getDocument(documentId);
	}

	@PUT
	@Path(value="/edit/{id}")
	//use badgerfish XML -> JSON encoding (http://wiki.open311.org/index.php?title=JSON_and_XML_Conversion#The_Badgerfish_Convention)
	@BadgerFish
	//consumes JSON and XML only
	@Consumes(value={MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML})
	//produces JSON, XML, HTML and Plain Text depending on the consumer
	@Produces(value={MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML, MediaType.TEXT_HTML, MediaType.TEXT_PLAIN})
	@Override
	public Document editDocument(@PathParam("id") int documentId, Revision revision) throws LoremIpsumException {
		return super.editDocument(documentId, revision);
	}


}
