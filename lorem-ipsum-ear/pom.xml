<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<!-- parent, with relative project path. -->
	<!-- can omit path if storing parent pom in some nexus/repository structure -->
	<parent>
		<groupId>org.lorem.ipsum</groupId>
		<artifactId>lorem-ipsum-parent</artifactId>
		<version>0.0.1-SNAPSHOT</version>
		<relativePath>..</relativePath>
	</parent>
	<!-- test-ear package -->
	<artifactId>lorem-ipsum-ear</artifactId>
	<!-- ensure package is ear -->
	<packaging>ear</packaging>
	<description>ear packaging for lorem-ipsum sample project</description>

	<!-- for the ear all dependencies must be bundled -->
	<dependencies>
		<!-- persistence -->
		<dependency>
			<groupId>org.lorem.ipsum</groupId>
			<artifactId>lorem-ipsum-persistence</artifactId>
		</dependency>

		<!-- ejb3 services -->
		<dependency>
			<groupId>org.lorem.ipsum</groupId>
			<artifactId>lorem-ipsum-ejb3</artifactId>
		</dependency>
		
		<!-- esb service -->
		<dependency>
			<groupId>org.lorem.ipsum</groupId>
			<artifactId>lorem-ipsum-esb</artifactId>
			<!-- packaging is not jar, but esb -->
			<type>esb</type>
		</dependency>
		
		<!-- business processes -->
		<dependency>
			<groupId>org.lorem.ipsum</groupId>
			<artifactId>lorem-ipsum-process</artifactId>
			<!-- packaging is not jar, but par -->
			<!-- type>par</type -->
			<type>jar</type>
		</dependency>		
		
		<!-- rest application with esb service integration -->
		<dependency>
			<groupId>org.lorem.ipsum</groupId>
			<artifactId>lorem-ipsum-rest</artifactId>
			<!-- packaging was not jar, but war -->
			<type>war</type>
		</dependency>
		
		<!-- soap web service application with esb service integration -->
		<dependency>
			<groupId>org.lorem.ipsum</groupId>
			<artifactId>lorem-ipsum-ws</artifactId>
			<!-- packaging was not jar, but war -->
			<type>war</type>
		</dependency>

		<!-- gwt front end application -->
		<dependency>
			<groupId>org.lorem.ipsum</groupId>
			<artifactId>lorem-ipsum-gwt</artifactId>
			<!-- packaging was not jar, but war -->
			<type>war</type>
		</dependency>				
		
		<!-- logging through slf4j but through log4j which is in jboss -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<!-- exclude log4j because it is provided by jboss -->
			<exclusions>
				<exclusion>
					<artifactId>log4j</artifactId>
					<groupId>log4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		
		<!-- common libraries -->
		<dependency>
			<groupId>org.lorem.ipsum</groupId>
			<artifactId>lorem-ipsum-common</artifactId>
			<exclusions>
				<!-- this is provided by the container but cannot be scoped to provided because doing so causes compile errors in lorem-ipsum-rest -->
				<exclusion>
					<groupId>org.hibernate.javax.persistence</groupId>
					<artifactId>hibernate-jpa-2.0-api</artifactId>				
				</exclusion>
			</exclusions>
		</dependency>
	</dependencies>

	<build>
		<resources>
			<!-- turn on resource filtering for profile configured data source -->
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
				<includes>
					<include>${org.lorem.ipsum.database.dsTemplateFile}</include>
				</includes>
				<targetPath>${project.build.directory}/${project.build.finalName}</targetPath>
			</resource>
		</resources>		
	
		<plugins>
			<!-- this is the maven ear plugin: generates an application.xml -->
			<plugin>
				<artifactId>maven-ear-plugin</artifactId>
				<version>2.7</version>
				<configuration>
					<!-- jboss configuration -->
					<jboss>
						<version>5</version>
						<!-- data source files -->
						<data-sources>
      						<data-source>${org.lorem.ipsum.database.dsTemplateFile}</data-source>
      					</data-sources>
					</jboss>
					<!-- where should lib files be bundled -->
					<defaultLibBundleDir>lib</defaultLibBundleDir>
					<!-- package dependencies for a war in a shared common lib folder, useful with multiple war archives that share dependencies -->
					<skinnyWars>true</skinnyWars>
					<!-- define sub-modules -->
					<modules>
						<!-- jca persistence unit -->
						<jarModule>
							<groupId>org.lorem.ipsum</groupId>
							<artifactId>lorem-ipsum-persistence</artifactId>
							<!-- bundle in root directory -->
							<bundleDir>/</bundleDir>
							<!-- make sure persistence is added to the application.xml, jar modules are not automatically added -->
							<includeInApplicationXml>true</includeInApplicationXml>
						</jarModule>	
						<!-- ejb3 services: very straightforward -->
						<jarModule>
							<groupId>org.lorem.ipsum</groupId>
							<artifactId>lorem-ipsum-ejb3</artifactId>
							<!-- bundle in root directory -->
							<bundleDir>/</bundleDir>
							<!-- make sure ejb3 is added to the application.xml, jar modules are not automatically added -->
							<includeInApplicationXml>true</includeInApplicationXml>
						</jarModule>
						<!-- using the generic jar module for the esb -->
						<jarModule>
							<groupId>org.lorem.ipsum</groupId>
							<artifactId>lorem-ipsum-esb</artifactId>
							<!-- bundle in root directory -->
							<bundleDir>/</bundleDir>
							<!-- make sure esb is added to the application.xml, jar modules are not automatically added -->
							<includeInApplicationXml>true</includeInApplicationXml>
						</jarModule>
						<!-- using the generic jar module for the par/business process -->
						<jarModule>
							<groupId>org.lorem.ipsum</groupId>
							<artifactId>lorem-ipsum-process</artifactId>
							<!-- bundle in root directory -->
							<bundleDir>/</bundleDir>
							<!-- make sure esb is added to the application.xml, jar modules are not automatically added -->
							<includeInApplicationXml>true</includeInApplicationXml>
						</jarModule>						
						<!-- rest war/web module -->
						<webModule>
							<groupId>org.lorem.ipsum</groupId>
							<artifactId>lorem-ipsum-rest</artifactId>
							<!-- set the context root for the rest application here -->
							<contextRoot>${org.lorem.ipsum.rest.root}</contextRoot>
						</webModule>
						<!-- soap web service war/web module -->
						<webModule>
							<groupId>org.lorem.ipsum</groupId>
							<artifactId>lorem-ipsum-ws</artifactId>
							<!-- set the context root for the rest application here -->
							<contextRoot>${org.lorem.ipsum.ws.root}</contextRoot>
						</webModule>						
						<!-- gwt ui war/web module -->
						<webModule>
							<groupId>org.lorem.ipsum</groupId>
							<artifactId>lorem-ipsum-gwt</artifactId>
							<!-- set the context root for the ui application here -->
							<contextRoot>${org.lorem.ipsum.ui.root}</contextRoot>
						</webModule>
					</modules>
					<!-- custom type mappings for esb to jar, otherwise the maven ear plugin won't recognize it -->
					<artifactTypeMappings>
						<artifactTypeMapping type="esb" mapping="jar" />
						<artifactTypeMapping type="par" mapping="jar" />
					</artifactTypeMappings>
				</configuration>
			</plugin>
			<!-- configure the maven resources plugin, so that it filters the resources in this project in a specific way -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-resources-plugin</artifactId>
				<version>2.5</version>
				<configuration>
					<!-- should NOT use default delimiters as they're already in use in the -ds files. -->
					<useDefaultDelimiters>false</useDefaultDelimiters>
					<!-- use @{com.example} instead -->
					<delimiters>
						<delimiter>@{*}</delimiter>
					</delimiters>					
				</configuration>
			</plugin>
		</plugins>
	</build>
</project>