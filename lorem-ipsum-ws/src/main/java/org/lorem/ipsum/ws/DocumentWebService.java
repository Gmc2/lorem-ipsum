package org.lorem.ipsum.ws;

import javax.jws.WebService;

import org.lorem.ipsum.exceptions.LoremIpsumException;
import org.lorem.ipsum.model.Document;
import org.lorem.ipsum.model.Revision;
import org.lorem.ipsum.services.AbstractEndpointProvider;
import org.lorem.ipsum.ws.exceptions.LoremIpsumWebServiceException;
import org.lorem.ipsum.ws.interfaces.IDocumentWebService;

@WebService(endpointInterface="org.lorem.ipsum.ws.interfaces.IDocumentWebService", serviceName="documentServiceEndpoint")
public class DocumentWebService extends AbstractEndpointProvider implements IDocumentWebService {

	@Override
	public int addDocument(String title, String htmlContent, String summary) throws LoremIpsumException {
		try {
			return super.addDocument(title, htmlContent, summary);			
		} catch(LoremIpsumException exception) {
			throw new LoremIpsumWebServiceException(exception);
		}
	}

	@Override
	public boolean deleteDocument(int documentId) throws LoremIpsumException {
		try {
			return super.deleteDocument(documentId);
		} catch(LoremIpsumException exception) {
			throw new LoremIpsumWebServiceException(exception);
		}
	}

	@Override
	public Document getDocument(int documentId) throws LoremIpsumException {
		try {
			return super.getDocument(documentId);
		} catch(LoremIpsumException exception) {
			throw new LoremIpsumWebServiceException(exception);
		}			
	}

	@Override
	public Document editDocument(int documentId, Revision revision) throws LoremIpsumException {
		try {
			return super.editDocument(documentId, revision);
		} catch(LoremIpsumException exception) {
			throw new LoremIpsumWebServiceException(exception);
		}			
	}

}
