package org.lorem.ipsum.ws.interfaces;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.lorem.ipsum.exceptions.LoremIpsumException;
import org.lorem.ipsum.exceptions.LoremIpsumFaultBean;
import org.lorem.ipsum.interfaces.IDocumentEndpointService;
import org.lorem.ipsum.model.Document;
import org.lorem.ipsum.model.Revision;

@WebService
@SOAPBinding(style=Style.DOCUMENT, parameterStyle=ParameterStyle.WRAPPED, use=Use.ENCODED)
@XmlSeeAlso({Document.class, Revision.class, LoremIpsumFaultBean.class})
public interface IDocumentWebService extends IDocumentEndpointService {

	@Override
	public int addDocument(@WebParam(name="title") String title, @WebParam(name="htmlContent") String htmlContent, @WebParam(name="summary") String summary) throws LoremIpsumException;
	
	@Override
	public Document editDocument(@WebParam(name="documentId") int documentId, @WebParam(name="revision") Revision revision) throws LoremIpsumException;
	
	@Override
	public boolean deleteDocument(@WebParam(name="documentId") int documentId) throws LoremIpsumException;
	
	@Override
	public Document getDocument(@WebParam(name="documentId") int documentId) throws LoremIpsumException;
	
}
