package org.lorem.ipsum.ws.exceptions;

import javax.xml.ws.WebFault;

import org.lorem.ipsum.exceptions.LoremIpsumException;
import org.lorem.ipsum.exceptions.LoremIpsumFaultBean;

@WebFault(name="loremIpsumWebServiceFault", faultBean="org.lorem.ipsum.exceptions.LoremIpsumFaultBean")
public class LoremIpsumWebServiceException extends LoremIpsumException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private LoremIpsumFaultBean wrapper = null;
	
	public LoremIpsumWebServiceException(LoremIpsumException exception) {
		super(exception);
		this.wrapper = new LoremIpsumFaultBean(exception);
	}
	
	public LoremIpsumWebServiceException(String message, LoremIpsumFaultBean faultBean, Throwable cause) {
		super(message, cause);
		if(cause instanceof LoremIpsumException) {
			this.wrapper = new LoremIpsumFaultBean((LoremIpsumException)cause);
		} else {
			this.wrapper = new LoremIpsumFaultBean();
			this.wrapper.setErrorMessage(message);
		}
	}

	public LoremIpsumWebServiceException(String message, LoremIpsumFaultBean faultBean) {
		super(message);
		this.wrapper = new LoremIpsumFaultBean();
		this.wrapper.setErrorMessage(message);
		
	}
	
	public LoremIpsumFaultBean getFaultInfo() {
		return this.wrapper;
	}

	
}
