package org.lorem.ipsum.ejb;

import javax.ejb.Remote;

import org.lorem.ipsum.interfaces.IDocumentInternalService;

@Remote
public interface RemoteDocumentPersistenceService extends IDocumentInternalService {

}
