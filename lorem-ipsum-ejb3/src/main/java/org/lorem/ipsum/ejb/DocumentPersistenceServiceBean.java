package org.lorem.ipsum.ejb;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.jboss.annotation.ejb.Depends;
import org.lorem.ipsum.model.Document;
import org.lorem.ipsum.persistence.DocumentPersistenceProvider;

//stateless enterprise bean
@Stateless
//bean managed transactions (BMT)
@TransactionManagement(TransactionManagementType.BEAN)
//depends on the data source binding for LoremIpsum (see the lorem ipsum datasource in lorem-ipsum-ear)
@Depends(value={"name=LoremIpsumDS,service=DataSourceBinding"})
/**
 * EJB provided implementation of IDocumentInteralService.  
 * 
 * @author chris
 *
 */
public class DocumentPersistenceServiceBean implements RemoteDocumentPersistenceService, LocalDocumentPersistenceService {

	//use the lorem-ipsum persistence unit, loaded in an EJB way, and injected via the JBoss container
	@PersistenceContext(unitName="lorem-ipsum-persistence")
	private EntityManager entityManager;

	@Resource
	//EJB context provided/injected by jboss
	//the reason this is here is because, within the context of a BMT (bean managed transaction) JPA call
	//there must be a transaction started/joined.  However, because of the way that JPA works in JBoss,
	//you cannot start/manage your own database transaction.  To work with the EJB and JPA together we use
	//a UserTransaction which can be started from the ejbContext object.
	private EJBContext ejbContext;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public int addDocument(Document document) {
		
		int documentId = -1;
		
		UserTransaction utx = this.beginTransaction();
		
		if(utx != null) {
			DocumentPersistenceProvider persistenceProvider = new DocumentPersistenceProvider(this.entityManager);
			documentId = persistenceProvider.addDocument(document);
		}
		
		this.commitTransaction(utx);
		
		return documentId;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean deleteDocument(int documentId) {
		
		boolean deleted = false;
		
		UserTransaction utx = this.beginTransaction();
		
		if(utx != null) {
			DocumentPersistenceProvider persistenceProvider = new DocumentPersistenceProvider(this.entityManager);			
			deleted = persistenceProvider.deleteDocument(documentId);
		}
		
		this.commitTransaction(utx);
		
		return deleted;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Document getDocument(int documentId) {
		
		Document document = null;
	
		UserTransaction utx = this.beginTransaction();
		
		if(utx != null) {
			DocumentPersistenceProvider persistenceProvider = new DocumentPersistenceProvider(this.entityManager);
			document = persistenceProvider.getDocument(documentId);
		}
		
		this.commitTransaction(utx);
			
		return document;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Document updateDocument(Document document) {
		UserTransaction utx = this.beginTransaction();
		
		if(utx != null) {
			DocumentPersistenceProvider persistenceProvider = new DocumentPersistenceProvider(this.entityManager);
			document = persistenceProvider.updateDocument(document);
		}
		
		this.commitTransaction(utx);
			
		return document;
	}
	
	/**
	 * Begin a UserTransaction from the EJB context
	 * 
	 * @return
	 */
	private UserTransaction beginTransaction() {
		UserTransaction utx = this.ejbContext.getUserTransaction();
		
		try {
			utx.begin();
		} catch (NotSupportedException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return utx;
	}
	
	/**
	 * Commit a transaction with the EJB is finished
	 * 
	 * @param utx
	 */
	private void commitTransaction(UserTransaction utx) {
		if(utx == null) return;
		
		try {
			utx.commit();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (RollbackException e) {
			e.printStackTrace();
		} catch (HeuristicMixedException e) {
			e.printStackTrace();
		} catch (HeuristicRollbackException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

}
