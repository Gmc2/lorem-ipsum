package org.lorem.ipsum.ejb;

import javax.ejb.Local;

import org.lorem.ipsum.interfaces.IDocumentInternalService;

@Local
public interface LocalDocumentPersistenceService extends IDocumentInternalService {

}
