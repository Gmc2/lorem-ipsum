# LOERM IPSUM
-------------

## Purpose
The purpose of Lorem Ipsum is mainly demonstrative.  It is not intended to do much more than demonstrate, clearly, some core maven, service-oriented, and JBoss concepts.  It is not intended to go beyond that in terms of complexity.  It is not intended to propose an architecture or layout.

### Goals:
> 1. To demonstrate how to create a maven project for complex artifacts
> 2. To demonstrate EAR packaging for JBoss
> 3. To demonstrate how to link different components together (REST, ESB/SOA-P, etc.)
> 4. To demonstrate various architectural techniques

### Non-Goals:
> 1. To provide any reference architecture.  The architecture in Lorem Ipsum is a little crazy on purpose.
> 2. To endorse any specific way of doing this as the "one true way"
> 3. To proclaim any one technology as the best/most appropriate 

## Technology Sample
Lorem Ipsum aims to demonstrate several technologies.  Provided here is a listing of the technologies in use, in progress, and planned.

Demonstrations implemented:

* Maven Building / Packaging

	* Profile Support
	
	* Resource Filtering
	
	* JAR, WAR, ESB, EAR Packaging

* REST implementatino with RESTEasy (RESTful XML and JSON)

    * JAXB and Jettison providers

    * Servlet Caching
    
    * Exception handling with the @Provider annotation
    
    * Maven/Jboss compatibility
    
* SOAP implementation with Apache CXF

	* Spring Configuration
	
	* Maven/JBoss compatibility
	
	* Exception handling with @WebFault and work-around for JBoss (LoremIpsumFaultBean)
	
* ESB (Specifically JBoss SOA-P 5.x compatible)

    * InVM Transport

    * Drools based ContentRouter

* EJB3

	* Stateless Session Bean / Bean Managed Transactions example
	
	* Provides local/remote persistence service
	
	* Decouples all other projects from lorem-ipsum-persistence (or at least allows them to decouple)
	
	* ESB Proxy example (decouples lorem-ipsum-ws and lorem-ipsum-ws from ESB API)
	
* JSR-303 Bean Validation (Hibernate Validator)

* Enterprise Archive support (EAR)

* Hibernate JPA based Persistence

    * JBoss Cache configuration (Inifnispan is only compatible with AS6 and higher, comments provided.)
    
* JBoss Data Source Configuration

    * HSQLDB support
    
    * Configured through Maven
    
    * Deployed as part of EAR
    
* JMeter Test Suite

In progress:

* GWT (2.4)

    * GWT Declarative XML UI

    * JSR-303 Bean Validation
    
Future plans:

* Management Beans
	
## Building
To build, simply use maven:

> maven clean package -P HSQLDB

Everything is packaged as an EAR when built.  The EAR distribution can be found in "lorem-ipsum-ear/target" when the build is successful.