package org.lorem.ipsum.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.lorem.ipsum.interfaces.IDocumentInternalService;
import org.lorem.ipsum.model.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Singleton for persistence support to any dependent projects.  Uses the "enum singleton" version of the singleton pattern.
 * 
 * @author chris
 *
 */
public class DocumentPersistenceProvider implements IDocumentInternalService {

	/**
	 * JPA Entity Manager
	 */
	private final EntityManager entityManager;
	
	/**
	 * Logger (SLF4J)
	 */
	private static final Logger logger = LoggerFactory.getLogger(DocumentPersistenceProvider.class);
	
	public DocumentPersistenceProvider() {
		//create entity manager
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("lorem-ipsum-persistence");
		this.entityManager = factory.createEntityManager();
	}
	
	public DocumentPersistenceProvider(EntityManager providedEntityManager) {
		//get entity manager
		this.entityManager = providedEntityManager;
	}

	@Override
	public int addDocument(Document document) {

		this.entityManager.persist(document);

		DocumentPersistenceProvider.logger.debug("Added new document: {}", document.getDocumentNumber());
		
		return document.getDocumentNumber();
	}

	@Override
	public boolean deleteDocument(int documentId) {
		
		boolean removed = false;
		
		//find document for removal
		Document document = this.entityManager.find(Document.class, documentId);
		
		try {
			if(document != null) {
				this.entityManager.remove(document);
				DocumentPersistenceProvider.logger.debug("Removed document: {}", documentId);
				removed = true;
			}  else {
				DocumentPersistenceProvider.logger.debug("Document {} is not currently seen as being managed, and cannot be removed", documentId);
			}
		} catch (Exception e) {
			DocumentPersistenceProvider.logger.error("Could not remove document: {}", documentId, e);
			e.printStackTrace();
		}
		
		return removed;
	}

	@Override
	public Document getDocument(int documentId) {
		
		Document document = this.entityManager.find(Document.class, documentId);
		
		if(document != null) {
			DocumentPersistenceProvider.logger.debug("Got document: {}", documentId);
		} else {
			DocumentPersistenceProvider.logger.debug("No document found for id: {}", documentId);
		}
		
		return document;
	}

	@Override
	public Document updateDocument(Document document) {
		this.entityManager.merge(document);

		DocumentPersistenceProvider.logger.debug("Updated document: {}", document.getDocumentNumber());
		
		return document;
	}

}
