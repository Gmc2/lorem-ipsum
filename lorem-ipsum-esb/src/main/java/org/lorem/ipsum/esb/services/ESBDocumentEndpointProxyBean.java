package org.lorem.ipsum.esb.services;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.jboss.soa.esb.client.ServiceInvoker;
import org.jboss.soa.esb.couriers.FaultMessageException;
import org.jboss.soa.esb.listeners.message.MessageDeliverException;
import org.jboss.soa.esb.message.Message;
import org.jboss.soa.esb.message.format.MessageFactory;
import org.jboss.soa.esb.services.registry.RegistryException;
import org.lorem.ipsum.exceptions.DocumentNotFoundException;
import org.lorem.ipsum.exceptions.LoremIpsumException;
import org.lorem.ipsum.messaging.Payload;
import org.lorem.ipsum.model.Document;
import org.lorem.ipsum.model.Revision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class ESBDocumentEndpointProxyBean implements ILocalESBDocumentEndpointProxy, IRemoteESBDocumentEndpointProxy {

	/**
	 * Logger (SLF4J)
	 */
	private static Logger logger = LoggerFactory.getLogger(ESBDocumentEndpointProxyBean.class);
	
	/**
	 * DocumentServices category for document based ESB services
	 */
	private String serviceCategory = "DocumentServices";
	
	@Override
	public int addDocument(String title, String htmlContent, String summary) throws LoremIpsumException {

		ServiceInvoker invoker = null;
		try {
			invoker = new ServiceInvoker(this.serviceCategory, "addDocument");
		} catch (MessageDeliverException e) {
			throw new LoremIpsumException("Could not add document.", e);
		}
		
		int documentId = -1;
		Exception exception = null;
		
		Message message = MessageFactory.getInstance().getMessage();
		if(invoker != null) {
			Document document = new Document();
			document.setTitle(title);
			document.setHtmlContents(htmlContent);
			document.setSummary(summary);
			
			//log debug info on document
			ESBDocumentEndpointProxyBean.logger.debug("Posted document: title={}, htmlContent={}, summary={}", new Object[]{title, htmlContent, summary});
			
			//add document to message body
			message.getBody().add(Payload.DOCUMENT.getKey(), document);
			
			//invoke, and catch exceptions
			try {
				//send message
				message = invoker.deliverSync(message, 5000);
				
				//get and decode documentId from response
				Object documentIdObject = message.getBody().get(Payload.DOCUMENT_ID.getKey());
				if(documentIdObject != null && documentIdObject instanceof Number) {
					Number number = (Number)documentIdObject;
					documentId = number.intValue();
				}
			} catch (FaultMessageException e) {
				exception = e;
			} catch (MessageDeliverException e) {
				exception = e;
			} catch (RegistryException e) {
				exception = e;
			}
		}
		
		//throw an exception if the document could not be added 
		if(documentId == -1) {
			if(exception != null) {
				throw new LoremIpsumException("Could not add document", exception);
			} else {
				throw new LoremIpsumException("Could not add document");
			}
		}
		
		//return document id so that the consumer can use it
		return documentId;
	}

	@Override
	public boolean deleteDocument(int documentId) throws LoremIpsumException  {
		ServiceInvoker invoker = null;
		try {
			invoker = new ServiceInvoker(this.serviceCategory, "deleteDocument");
		} catch (MessageDeliverException e) {
			throw new LoremIpsumException("Could not delete document.", e);
		}
		
		Exception exception = null;
		Collection<Exception> exceptions = null;
		boolean deleted = false;
		
		Message message = MessageFactory.getInstance().getMessage();
		if(invoker != null) {
			//log debug info on document retrieval
			ESBDocumentEndpointProxyBean.logger.debug("Deleting document: documentId={}", documentId);
			
			//add document to message body
			message.getBody().add(Payload.DOCUMENT_ID.getKey(), documentId);
			
			//invoke, and catch exceptions
			try {
				//send message
				message = invoker.deliverSync(message, 5000);
				
				//get exceptions from deleting
				Object collectionObject = message.getBody().get(Payload.EXCEPTIONS.getKey());
				if(collectionObject != null && collectionObject instanceof Collection) {
					@SuppressWarnings("unchecked")
					Collection<Exception> collection = (Collection<Exception>)collectionObject;
					exceptions = collection;
				}								
				
				//if no exceptions, deleted is true
				if(exceptions == null || exceptions.isEmpty()) {
					deleted = true;
				}
			} catch (FaultMessageException e) {
				exception = e;
			} catch (MessageDeliverException e) {
				exception = e;
			} catch (RegistryException e) {
				exception = e;
			}
		}
		
		//throw exception when the document could not be deleted
		if(exception != null) {
			throw new LoremIpsumException("Could not delete document " + documentId, exception);
		}
		
		return deleted;
	}

	@Override
	public Document getDocument(int documentId) throws LoremIpsumException {
		ServiceInvoker invoker = null;
		try {
			invoker = new ServiceInvoker(this.serviceCategory, "getDocument");
		} catch (MessageDeliverException e) {
			throw new LoremIpsumException("Could not get document.", e);
		}
		
		Document document = null;
		Exception exception = null;
		
		Message message = MessageFactory.getInstance().getMessage();
		if(invoker != null) {
			//log debug info on document retrieval
			ESBDocumentEndpointProxyBean.logger.debug("Retrieving document: documentId={}", documentId);
			
			//add document to message body
			message.getBody().add(Payload.DOCUMENT_ID.getKey(), documentId);
			
			//invoke, and catch exceptions
			try {
				//send message
				message = invoker.deliverSync(message, 5000);
			} catch (FaultMessageException e) {
				exception = e;
			} catch (MessageDeliverException e) {
				exception = e;
			} catch (RegistryException e) {
				exception = e;
			}
			
			//get document from message, if it is not null
			if(message.getBody().get(Payload.DOCUMENT.getKey()) != null) {
				document = (Document)message.getBody().get(Payload.DOCUMENT.getKey());
			}
		}
		
		//throw exception if the document could not be found
		if(document == null) {
			if(exception != null) {
				throw new DocumentNotFoundException("Could not find document " + documentId, exception);
			} else {
				throw new DocumentNotFoundException("Could not find document " + documentId);
			}
		}
		
		return document;	
	}

	@Override
	public Document editDocument(int documentId, Revision revision) throws LoremIpsumException {
		ServiceInvoker invoker = null;
		try {
			invoker = new ServiceInvoker(this.serviceCategory, "editDocument");
		} catch (MessageDeliverException e) {
			throw new LoremIpsumException("Could not edit document.", e);
		}
		
		Document document = null;
		Exception exception = null;
		
		Message message = MessageFactory.getInstance().getMessage();
		if(invoker != null) {
			//log debug info on revision
			ESBDocumentEndpointProxyBean.logger.debug("Revising document: documentId={}, edit count={}", new Object[]{documentId, revision.getEdits().size()});
			
			//add document to message body
			message.getBody().add(Payload.DOCUMENT_ID.getKey(), documentId);
			message.getBody().add(Payload.REVISION.getKey(), revision);
			
			//invoke, and catch exceptions
			try {
				//send message
				message = invoker.deliverSync(message, 5000);
				
				//get document from message
				document = (Document)message.getBody().get(Payload.DOCUMENT.getKey());
			} catch (FaultMessageException e) {
				exception = e;
			} catch (MessageDeliverException e) {
				exception = e;
			} catch (RegistryException e) {
				exception = e;
			}
		}
		
		//exception when document cannot be edited
		if(document == null || exception != null) {
			if(exception != null) {
				throw new DocumentNotFoundException("Could not find document " + documentId, exception);
			} else {
				throw new DocumentNotFoundException("Could not find document " + documentId);
			}
		}
		
		return document;	
		
	}

}
