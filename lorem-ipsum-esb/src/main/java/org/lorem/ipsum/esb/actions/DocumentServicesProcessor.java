package org.lorem.ipsum.esb.actions;

import org.jboss.soa.esb.actions.annotation.Process;
import org.jboss.soa.esb.lifecycle.annotation.Initialize;
import org.jboss.soa.esb.message.Message;
import org.lorem.ipsum.interfaces.IDocumentEndpointService;
import org.lorem.ipsum.interfaces.IDocumentInternalService;
import org.lorem.ipsum.logic.AbstractUsesDocumentServices;
import org.lorem.ipsum.messaging.Payload;
import org.lorem.ipsum.model.Document;
import org.lorem.ipsum.model.Revision;
import org.lorem.ipsum.utility.DocumentUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides methods similar to the document services endpoints inside of the ESB
 * 
 * @see IDocumentEndpointService
 * @see IDocumentInternalService
 * @author chris
 *
 */
public class DocumentServicesProcessor extends AbstractUsesDocumentServices<IDocumentInternalService> {

	/**
	 * Logger
	 */
	private Logger logger;
	
	/**
	 * JNDI name of service to look up, in this case one that implements IDocumentInternalService and provides persistence
	 * 
	 */
	private final String jndiServiceName = "lorem-ipsum-ear-0.0.1-SNAPSHOT/DocumentPersistenceServiceBean/remote";
	
	@Initialize
	public void configureDocumentServices() {
		//create logger for errors
		this.logger = LoggerFactory.getLogger(DocumentServicesProcessor.class);
	}
	
	//esb process method annotation
	@Process
	/**
	 * Extracts a document from the DOCUMENT {@link Payload} of the {@link Message} and 
	 * saves it to the persistence layer
	 * 
	 * @param message
	 * @return
	 */
	public Message addDocument(Message message) {
		
		Document document = (Document)message.getBody().get(Payload.DOCUMENT.getKey());
		
		IDocumentInternalService documentService = this.getDocumentService(this.jndiServiceName);
		int documentId = documentService.addDocument(document);
		
		message.getBody().add(Payload.DOCUMENT_ID.getKey(), documentId);
		
		return message;
	}

	//esb process method annotation
	@Process
	/**
	 * Extracts a document from the DOCUMENT_ID {@link Payload} of the {@link Message} and 
	 * returns the document with that ID from the persistence layer
	 * 
	 * @param message
	 * @return
	 */
	public Message getDocument(Message message) {

		Integer documentId = (Integer)message.getBody().get(Payload.DOCUMENT_ID.getKey());
		
		if(documentId != null) {
			IDocumentInternalService documentService = this.getDocumentService(this.jndiServiceName);
			Document document = documentService.getDocument(documentId);
			
			if(document != null) {
				message.getBody().add(Payload.DOCUMENT.getKey(), document);
			}
		}	
		
		return message;
	}
	
	//esb process method annotation
	@Process
	/**
	 * Extracts a document from the DOCUMENT {@link Payload} of the {@link Message} a revision from 
	 * the REVISION {@link Payload} and uses the DocumentUtility to create an updated document and
	 * then stores the updated document back in the Message.
	 * 
	 * @param message
	 * @return
	 */
	public Message editDocument(Message message) {
		
		Document document = (Document)message.getBody().get(Payload.DOCUMENT.getKey());
		Revision revision = (Revision)message.getBody().get(Payload.REVISION.getKey());
		
		DocumentUtility.revise(document, revision);
		
		message.getBody().add(Payload.DOCUMENT.getKey(), document);
		
		return message;
	}
	
	//esb process method annotation
	@Process
	/**
	 * Extracts a document from the DOCUMENT_ID {@link Payload} of the {@link Message} and 
	 * instructs the persistence layer to delete that document
	 * 
	 * @param message
	 * @return
	 */	
	public Message deleteDocument(Message message) {

		Integer documentId = (Integer)message.getBody().get(Payload.DOCUMENT_ID.getKey());
		
		if(documentId != null) {
			IDocumentInternalService documentService = this.getDocumentService(this.jndiServiceName);
			documentService.deleteDocument(documentId);
		}	
		
		return message;
	}
	
	//esb process method annotation
	@Process
	/**
	 * Extracts a document from the DOCUMENT {@link Payload} of the {@link Message} and 
	 * updates the persistence layer with that document
	 * 
	 * @param message
	 * @return
	 */
	public Message updateDocument(Message message) {
		
		Document document = (Document)message.getBody().get(Payload.DOCUMENT.getKey());
		
		IDocumentInternalService documentService = this.getDocumentService(this.jndiServiceName);
		document = documentService.updateDocument(document);
		
		message.getBody().add(Payload.DOCUMENT.getKey(), document);
		
		return message;
	}
	
	
}
