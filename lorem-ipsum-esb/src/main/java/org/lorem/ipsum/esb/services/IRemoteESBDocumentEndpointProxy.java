package org.lorem.ipsum.esb.services;

import javax.ejb.Remote;

import org.lorem.ipsum.interfaces.IDocumentEndpointService;

@Remote
public interface IRemoteESBDocumentEndpointProxy extends IDocumentEndpointService {

}
