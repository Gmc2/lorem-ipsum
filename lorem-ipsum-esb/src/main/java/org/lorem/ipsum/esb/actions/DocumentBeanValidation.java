package org.lorem.ipsum.esb.actions;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.jboss.soa.esb.actions.annotation.Process;
import org.jboss.soa.esb.message.Message;
import org.lorem.ipsum.messaging.Payload;
import org.lorem.ipsum.model.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Custom ESB process to validate messages
 * 
 * @author chris
 *
 */
public class DocumentBeanValidation {

	private static Logger logger = LoggerFactory.getLogger(DocumentBeanValidation.class);
	
	//esb annotation on process message
	@Process
	/**
	 * Ingest a message, extract the document object, and perform validation
	 * 
	 * @param message
	 * @return
	 */
	public Message doBeanValidation(Message message) {
		
		//get validator
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		
		//get bean from message
		Document document = (Document)message.getBody().get(Payload.DOCUMENT.getKey());
		
		//validate
		Set<ConstraintViolation<Document>> violations = validator.validate(document);
		
		//log
		DocumentBeanValidation.logger.debug("Validated bean with {} violations.", violations.size());
		for(ConstraintViolation<Document> violation : violations) {
			DocumentBeanValidation.logger.debug("Violation on path {} of type {} with message \"{}\"", new Object[]{violation.getPropertyPath().toString(), violation.getConstraintDescriptor().getAnnotation().annotationType().getName(), violation.getMessage()});
		}
		
		//save violations to message (even if empty)
		message.getBody().add(Payload.CONSTRAINT_VIOLATIONS.getKey(), violations);
		
		return message;
	}
	
}
