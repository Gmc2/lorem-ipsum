package org.lorem.ipsum.esb.services;

import javax.ejb.Local;

import org.lorem.ipsum.interfaces.IDocumentEndpointService;

@Local
public interface ILocalESBDocumentEndpointProxy extends IDocumentEndpointService {

}
